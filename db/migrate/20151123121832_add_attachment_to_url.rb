class AddAttachmentToUrl < ActiveRecord::Migration
  def change
    add_attachment :chat_message_urls, :image
  end
end
