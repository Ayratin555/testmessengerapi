class AddUrlToVideo < ActiveRecord::Migration
  def change
    add_column :chat_message_videos, :url, :string
  end
end
