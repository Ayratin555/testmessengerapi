class CreateUrlForMessages < ActiveRecord::Migration
  def change
    create_table :chat_message_urls do |t|
      t.string :url
      t.string :name
      t.string :description
      t.timestamps null: false
    end

    add_reference :chat_message_urls, :chat_message, foreign_key: true
  end
end
