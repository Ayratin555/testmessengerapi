class AddUserToChats < ActiveRecord::Migration
  def change
    add_reference :chats, :user, foreign_key: true
  end
end
