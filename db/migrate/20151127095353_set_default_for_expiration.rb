class SetDefaultForExpiration < ActiveRecord::Migration
  def change
    change_column :sessions, :expiration, :interval, :default => 1800
  end
end
