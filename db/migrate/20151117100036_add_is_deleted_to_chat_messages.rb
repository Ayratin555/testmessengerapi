class AddIsDeletedToChatMessages < ActiveRecord::Migration
  def change
    add_column :chat_messages, :is_deleted, :boolean, :default => false, :null => false
  end
end
