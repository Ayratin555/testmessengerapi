class AddReferenceImage < ActiveRecord::Migration
  def change
    add_reference :chat_message_images, :chat_message, foreign_key: true
  end
end
