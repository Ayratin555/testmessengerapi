class CreateChatVideo < ActiveRecord::Migration
  def change
    create_table :chat_videos do |t|
      t.attachment :video
      t.timestamps null: false
    end

    add_reference :chat_videos, :user, foreign_key: true
    add_reference :chat_videos, :chat, foreign_key: true
  end
end
