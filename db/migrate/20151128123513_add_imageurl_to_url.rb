class AddImageurlToUrl < ActiveRecord::Migration
  def change
    add_column :chat_message_urls, :url, :string
  end
end
