class AddEmailAndPasswordToUsers < ActiveRecord::Migration
  def change
    add_column :users, :password, :string, :null => false
    add_column :users, :email, :string, :null => false
  end
end
