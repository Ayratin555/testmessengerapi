class RenameImageurlInUrl < ActiveRecord::Migration
  def change
    rename_column :chat_message_urls, :url, :image_url
  end
end
