class ChangeApiKeyNameToToken < ActiveRecord::Migration
  def change
    rename_column :users, :api_key, :token
  end
end
