class AddUrlToImages < ActiveRecord::Migration
  def change
    add_column :chat_message_images, :url, :string
  end
end
