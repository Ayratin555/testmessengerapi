class AddTimestampsToVideosAndImages < ActiveRecord::Migration
  def self.up # Or `def up` in 3.1
    change_table :chat_message_images do |t|
      t.timestamps
    end
    change_table :chat_message_videos do |t|
      t.timestamps
    end
  end
  def self.down # Or `def down` in 3.1
    remove_column :chat_message_images, :created_at
    remove_column :chat_message_images, :updated_at
    remove_column :chat_message_videos, :created_at
    remove_column :chat_message_videos, :updated_at
  end
end
