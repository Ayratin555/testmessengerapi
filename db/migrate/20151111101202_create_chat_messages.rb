class CreateChatMessages < ActiveRecord::Migration
  def change
    create_table :chat_messages do |t|
      t.string :body
      t.timestamps null: false
    end

    add_reference :chat_messages, :user, foreign_key: true
    add_reference :chat_messages, :chat, foreign_key: true
  end
end
