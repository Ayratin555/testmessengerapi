class CreateChatParticipants < ActiveRecord::Migration
  def change
    create_table :chat_participants do |t|
      t.timestamps null: false
      t.belongs_to :user
      t.belongs_to :chat
    end
  end
end
