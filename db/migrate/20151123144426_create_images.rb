class CreateImages < ActiveRecord::Migration
  def change
    create_table :chat_message_images do |t|
      t.attachment :image
    end
  end
end
