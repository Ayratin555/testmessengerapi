class AddExpirationForToken < ActiveRecord::Migration
  def change
    add_column :sessions, :expiration, :interval
  end
end
