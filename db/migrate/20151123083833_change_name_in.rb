class ChangeNameIn < ActiveRecord::Migration
  def change
    rename_column :chat_message_urls, :url, :url_adr
  end
end
