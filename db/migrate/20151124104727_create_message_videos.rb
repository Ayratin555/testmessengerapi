class CreateMessageVideos < ActiveRecord::Migration
  def change
    create_table :chat_message_videos do |t|
      t.attachment :video
    end

    add_reference :chat_message_videos, :chat_message, foreign_key: true
  end
end
