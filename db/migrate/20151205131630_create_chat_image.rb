class CreateChatImage < ActiveRecord::Migration
  def change
    create_table :chat_images do |t|
      t.attachment :image
      t.timestamps null: false
    end

    add_reference :chat_images, :user, foreign_key: true
    add_reference :chat_images, :chat, foreign_key: true
  end
end
