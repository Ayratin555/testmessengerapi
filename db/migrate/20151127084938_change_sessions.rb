class ChangeSessions < ActiveRecord::Migration
  def change
    remove_column :sessions, :path
    add_column :sessions, :token, :string
  end
end
