require 'test_helper'
require 'ffaker'

class MessageScraperServiceTest < Minitest::Test
  def setup
  end

  def test_should_scrape_valid_urls_with_html
    body = "https://www.google.ru/" + " " + "https://www.yandex.ru/"
    params = { body: body}
    attachments = MessageScraperService.attachments(params)
    assert_equal attachments[:urls_attributes].length, 2
    assert_empty attachments[:images_attributes]
  end

  def test_should_scrape_name_description_and_image_url
    params = { body: "https://www.google.ru/" }
    attachments = MessageScraperService.attachments(params)
    refute_empty attachments[:urls_attributes][0]['name']
    refute_empty attachments[:urls_attributes][0]['description']
    refute_empty attachments[:urls_attributes][0]['image_url']
    assert_empty attachments[:images_attributes]
  end

  def test_shouldnt_scrape_message_without_urls
    params = { body: FFaker::Lorem.paragraph }
    attachments = MessageScraperService.attachments(params)
    assert_empty attachments[:urls_attributes]
    assert_empty attachments[:images_attributes]
  end

  def test_should_scrape_valid_url_with_image_urls
    body = "http://www.dotatalk.com/wp-content/uploads/2013/09/Logo-DOTA27.jpg" + ' ' +
           "http://vega-squadron.com/sites/default/files/uploads/HighArT/dota.png"
    params = { body: body }
    attachments = MessageScraperService.attachments(params)
    assert_equal attachments[:images_attributes].length, 2
    assert_empty attachments[:urls_attributes]
  end

  def test_should_scrape_valid_url_with_video_urls
    body = "http://www.engr.colostate.edu/me/facil/dynamics/files/drop.avi" + ' ' +
        "http://www.engr.colostate.edu/me/facil/dynamics/files/bird.avi"
    params = { body: body }
    attachments = MessageScraperService.attachments(params)
    assert_equal attachments[:videos_attributes].length, 2
    assert_empty attachments[:urls_attributes]
  end

  def test_should_scrpape_urls_of_different_kind
    body = "http://www.engr.colostate.edu/me/facil/dynamics/files/drop.avi" + ' ' +
        "http://www.dotatalk.com/wp-content/uploads/2013/09/Logo-DOTA27.jpg" + ' ' +
        "https://www.google.ru/"
    params = { body: body }
    attachments = MessageScraperService.attachments(params)
    assert_equal attachments[:videos_attributes].length, 1
    assert_equal attachments[:images_attributes].length, 1
    assert_equal attachments[:urls_attributes].length, 1
  end
end