require 'ffaker'

FactoryGirl.define do
  factory :participant, class: 'Chat::Participant' do
    chat_id :chat_id
    user_id :user_id
  end
end