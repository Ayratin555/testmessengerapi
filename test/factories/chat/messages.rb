require 'ffaker'

FactoryGirl.define do
  factory :message, class: 'Chat::Message' do
    body { FFaker::Lorem.paragraph }
    chat_id :chat_id
    user_id :user_id
    is_deleted false
  end
end