require 'ffaker'

FactoryGirl.define do
  factory :chat do
    name { FFaker::Lorem.word }
  end
end