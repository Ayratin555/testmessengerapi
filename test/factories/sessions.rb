require 'ffaker'

FactoryGirl.define do
  factory :session do
    ip_address { FFaker::Internet.ip_v4_address }
  end
end