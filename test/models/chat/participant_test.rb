require 'test_helper'

class Chat::ParticipantTest < ActiveSupport::TestCase
  setup do
    user = create(:user)
    @valid_params = { user_id: user.id,
                      chat_id: create(:chat).id }
  end

  test "should validate participant" do
    message = Chat::Participant.new @valid_params
    assert message.valid?
  end

  test "should be invalid without user" do
    params = @valid_params.clone
    params.delete :user_id
    participant = Chat::Participant.new params
    refute participant.valid?, "Can't be valid without body"
  end

  test "should be invalid without chat" do
    params = @valid_params.clone
    params.delete :chat_id
    participant = Chat::Participant.new params
    refute participant.valid?, "Can't be valid without body"
  end
end