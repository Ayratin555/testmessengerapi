require 'test_helper'

class Chat::MessageTest < ActiveSupport::TestCase
  setup do
    user = create(:user)
    @valid_params = { body: FFaker::Lorem.paragraph,
                      user_id: user.id,
                      chat_id: create(:chat).id }
  end

  test "should validate chat" do
    message = Chat::Message.new @valid_params
    assert message.valid?
  end

  test "should be invalid without body" do
    params = @valid_params.clone
    params.delete :body
    message = Chat::Message.new params
    refute message.valid?, "Can't be valid without body"
  end

  test "should be invalid without user" do
    params = @valid_params.clone
    params.delete :user_id
    message = Chat::Message.new params
    refute message.valid?, "Can't be valid without user"
  end

  test "should be invalid without chat" do
    params = @valid_params.clone
    params.delete :chat_id
    message = Chat::Message.new params
    refute message.valid?, "Can't be valid without chat"
  end

  test "should return all messages by specific user" do
    create_list(:message, 5, user_id: @valid_params[:user_id], chat_id: @valid_params[:chat_id])
    create_list(:message, 5, user_id: create(:user).id, chat_id: @valid_params[:chat_id])
    messages = Chat::Message.all.by_user(@valid_params[:user_id])
    assert_equal(messages.length, 5)
    messages.each{|mes| assert_equal(mes[:user_id], @valid_params[:user_id])}
  end

  test "should return all deleted messages" do
    create_list(:message, 5, user_id: @valid_params[:user_id], chat_id: @valid_params[:chat_id])
    create_list(:message, 5, user_id: @valid_params[:user_id], chat_id: @valid_params[:chat_id], is_deleted: true)
    messages = Chat::Message.all.is_deleted(true)
    assert_equal(messages.length, 5)
    messages.each{|mes| assert_equal(mes[:is_deleted], true)}
  end

  test "should return messages by date" do
    time1 = Time.now
    time2 = Time.new(2013,01,06, 11, 25, 00, "-08:00")
    create_list(:message, 5, user_id: @valid_params[:user_id], chat_id: @valid_params[:chat_id], created_at: time1)
    create_list(:message, 5, user_id: @valid_params[:user_id], chat_id: @valid_params[:chat_id], created_at: time2)
    messages = Chat::Message.all.equal_to_date(Time.now)
    assert_equal(messages.length, 5)
    messages.each{|mes| assert_equal(mes[:created_at].to_i, time1.to_i) }
  end
end