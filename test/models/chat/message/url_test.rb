require 'test_helper'
require 'ffaker'

class Chat::Message::UrlTest < ActiveSupport::TestCase
  setup do
    user = create(:user)
    chat = create(:chat)
    message = create(:message, user_id: user.id, chat_id: chat.id)
    @valid_params = { chat_message_id: message.id,
                      url_adr: FFaker::Internet.http_url}
  end

  test 'should validate url' do
    url = Chat::Message::Url.new @valid_params
    assert url.valid?
  end

  test 'should invalidate url without message' do
    params = @valid_params.clone
    params.delete :chat_message_id
    url = Chat::Message::Url.new params
    refute url.valid?
  end

  test 'should invalidate url without url_adr' do
    params = @valid_params.clone
    params.delete :url_adr
    url = Chat::Message::Url.new params
    refute url.valid?
  end
end