require 'test_helper'
require 'ffaker'

class UserTest < ActiveSupport::TestCase
  setup do
    @valid_params = { name: FFaker::Name.name,
                      email: FFaker::Internet.email,
                      password: FFaker::Internet.password }
  end

  test "should validate user" do
    user = User.new @valid_params
    assert user.valid?, "Can't create with valid params: #{user.errors.messages}"
  end

  test "should be invalid without name" do
    params = @valid_params.clone
    params.delete :name
    user = User.new params
    refute user.valid?, "Can't be valid without email"
    assert user.errors[:email], "Missing error when without email"
  end

  test "should be invalid with taken name" do
    old_user = create(:user)
    new_user = User.new(:name => old_user.name)
    refute new_user.valid?, "Can't be valid with taken name"
  end
end