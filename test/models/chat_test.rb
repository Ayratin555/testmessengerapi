require 'test_helper'
require 'ffaker'

class ChatTest < ActiveSupport::TestCase
  setup do
    @creator = create(:user)
    @chat = create(:chat)
    @valid_params = { :name => FFaker::Internet.name }
  end

  test "should delete all dependent messages" do
    create(:participant, :user_id => @creator.id, :chat_id => @chat.id)
    create_list(:message, 10, :chat_id => @chat.id, :user_id => @creator.id)
    @chat.destroy
    messages= Chat::Message.where(chat_id: @chat.id)
    assert_empty messages
  end

  test "should delete all dependent participants" do
    user = create(:user)
    create(:participant, :chat_id => @chat.id, :user_id => user.id)
    @chat.destroy
    participant = Chat::Participant.where(chat_id: @chat.id)
    assert_empty participant
  end

  test "should validate chat" do
    chat = Chat.new(@valid_params)
    assert chat.valid?, "Can't create with valid params: #{chat.errors.messages}"
  end

  test "should be invalid without name" do
    params = @valid_params.clone
    params.delete :name
    chat = Chat.new(params)
    refute chat.valid?, "Can't be valid without name"
  end
end