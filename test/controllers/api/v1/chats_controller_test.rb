require 'test_helper'
require 'ffaker'

class Api::V1::ChatsControllerTest < ActionController::TestCase

  setup do
    @user = create(:user)
    @session = create(:session, :user_id => @user.id)
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Token.encode_credentials(@session.token)
  end

  test "should create new chat" do
    name = FFaker::Lorem.word
    post :create, name: name
    assert_response :success
    assert_not_nil Chat.find_by_name(name)
  end

  test "shouldn't create chat with wrong parameters" do
    post :create, wrong_parameter: FFaker::Lorem.word
    assert_response :unprocessable_entity
  end

  test "should respond with chat and its associations" do
    chat1 = create(:chat)
    chat2 = create(:chat)
    create(:participant, :chat_id => chat1.id, :user_id => @user.id)
    create(:participant, :chat_id => chat2.id, :user_id => @user.id)
    get :index
    hash = JSON.parse(response.body)
    assert_not_empty hash['chats'][0]['participants']
    assert_response :success
  end

  test "should paginate chats" do
    (0..70).each do
      chat = create(:chat)
      create(:participant, :chat_id => chat.id, :user_id => @user.id)
    end
    get :index, page: 2
    hash = JSON(response.body)
    assert_equal(hash['chats'].length, 25)
    assert_response :success
  end

  test "should add user to participants" do
    new_user = create(:user)
    new_session = create(:session, :user_id => new_user.id)
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Token.encode_credentials(new_session.token)
    post :create, name: FFaker::Lorem.word
    creator = Chat::Participant.find_by_user_id(new_user.id)
    assert_not_nil creator
    assert_response :success
  end

  test "shouldnt authenticate user if token is expired" do
    new_user = create(:user)
    new_session = create(:session, :user_id => new_user.id, :created_at => Time.new(2013,01,06, 11, 25, 00, "-08:00"))
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Token.encode_credentials(new_session.token)
    post :create, name: FFaker::Lorem.word
    assert_response 401
  end
end

