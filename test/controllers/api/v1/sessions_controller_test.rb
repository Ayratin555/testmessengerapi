require 'test_helper'
require 'ffaker'

class Api::V1::SessionsControllerTest < ActionController::TestCase
  setup do
    @user = create(:user)
  end

  test "should create new session" do
    post :create, email: @user.email, password: @user.password
    assert_response :success
  end

  test "should not create new session if user doesn't exist" do
    post :create, email: FFaker::Internet.email, password: FFaker::Internet.password
    assert_response :not_found
  end

  test "should not return session if password wrong" do
    post :create, email: @user.email, password: FFaker::Internet.password
    assert_response 404
  end

  test "should destroy session" do
    session = create(:session, :user_id => @user.id)
    delete :destroy, email: @user.email, password: @user.password, id: session.token
    db_session = Session.find_by_token(session.token)
    assert_nil db_session
    assert_response :no_content
  end

  test "shouldn't destroy anything" do
    delete :destroy, id: '555'
    assert_response 404
  end
end