require 'test_helper'
require 'ffaker'


class Api::V1::Chat::MessagesControllerTest < ActionController::TestCase

  setup do
    @chat = create(:chat)
    @user = create(:user)
    @session = create(:session, :user_id => @user.id)
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Token.encode_credentials(@session.token)
    @message = create(:message, user_id: @user.id, chat_id: @chat.id)
    create(:participant, user_id: @user.id, chat_id: @chat.id)
  end

  test "should show all messages of one chat" do
    n=5
    create_list(:message, n, user_id: @user.id, chat_id: @chat.id)
    get :index, chat_id: @chat.id
    parsed_response = JSON.parse response.body
    assert_response :success
    assert_equal(parsed_response['messages'].size, n + 1)
  end

  test "should send message to chat" do
    post :create, chat_id: @chat.id, body: FFaker::Lorem.paragraph
    assert_response :success
  end

  test "shouldn't send message to chat if user is not participant" do
    not_participant = create(:user)
    session = create(:session, :user_id => not_participant.id)
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Token.encode_credentials(session.token)
    post :create, chat_id: @chat.id, body: FFaker::Lorem.paragraph
    assert_response :unauthorized
  end

  test "shouldn't return messages if no messages this day" do
    date = Time.new(2013,01,06, 11, 25, 00, "-08:00")
    create_list(:message, 20, user_id: @user.id, chat_id: @chat.id, created_at: Time.now.to_i)
    get :index, chat_id: @chat.id, date: date.to_i
    parse_response = JSON.parse response.body
    assert_empty parse_response['messages']
    assert_response :success
  end

  test "should return messages by date" do
    date = Time.new(2013,01,06, 11, 25, 00, "-08:00")
    create_list(:message, 20, user_id: @user.id, chat_id: @chat.id, created_at: date)
    get :index, chat_id: @chat.id, date: date.to_i
    parse_response = JSON.parse response.body
    assert_not_empty parse_response['messages']
    assert_response :success
  end

  test "should update current user's message" do
    new_body = FFaker::Lorem.paragraph
    patch :update, chat_id: @chat.id, id: @message.id, body: new_body
    updated_message = Chat::Message.find(@message.id)
    assert_response :success
    assert_equal(updated_message.body, new_body)
  end

  test "shouldn't update another user's message" do
    user = create(:user)
    create(:participant, :chat_id => @chat.id, :user_id => user.id)
    session = create(:session, :user_id => user.id)
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Token.encode_credentials(session.token)
    new_body = FFaker::Lorem.paragraph
    patch :update, chat_id: @chat.id, id: @message.id, body: new_body
    assert_response :unauthorized
  end

  test "should mark as deleted" do
    delete :destroy, id: @message.id, chat_id: @chat.id
    deleted_message = Chat::Message.find(@message.id)
    assert_equal(deleted_message.is_deleted, true)
    assert_response :success
  end

  test "shouldn't show current_user's deleted messages" do
    delete :destroy, chat_id: @chat.id, id: @message.id
    get :index, chat_id: @chat.id, deleted: '0'
    json_response = JSON.parse response.body
    assert_empty json_response['messages']
    assert_response :success
  end

  test "shouldn't show all messages" do
    delete :destroy, chat_id: @chat.id, id: @message.id
    get :index, chat_id: @chat.id
    json_response = JSON.parse response.body
    assert_not_empty json_response['messages']
    assert_response :success
  end

  test 'should show deleted messages' do
    get :index, chat_id: @chat.id
    delete :destroy, chat_id: @chat.id, id: @message.id
    get :index, chat_id: @chat.id, deleted: '1'
    json_response = JSON.parse response.body
    assert_not_empty json_response['messages']
    assert_response :success
  end

  test 'should add html urls to message' do
    body = "https://www.google.ru/" + ' ' + "https://yandex.ru/" + ' ' + "http://www.bing.com/"
    post :create, chat_id: @chat.id, body: body
    response_parsed = JSON.parse response.body
    assert_response :success
    assert_equal response_parsed['message']['urls'].length, 3
  end

  test 'should add image urls to message' do
    body = "http://www.dotatalk.com/wp-content/uploads/2013/09/Logo-DOTA27.jpg" + ' ' +
        "http://vega-squadron.com/sites/default/files/uploads/HighArT/dota.png"
    post :create, chat_id: @chat.id, body: body
    response_parsed = JSON.parse response.body
    assert_response :success
    assert_equal response_parsed['message']['images'].length, 2
    assert_not_empty response_parsed['message']['images']
  end

  test 'should add thumb image to message' do
    body = "http://www.dotatalk.com/wp-content/uploads/2013/09/Logo-DOTA27.jpg"
    post :create, chat_id: @chat.id, body: body
    response_parsed = JSON.parse response.body
    assert_response :success
    assert_not_empty response_parsed['message']['images'][0]['image_thumb']
  end

  test 'should add videos to message' do
    body = "http://www.engr.colostate.edu/me/facil/dynamics/files/drop.avi"
    post :create, chat_id: @chat.id, body: body
    response_parsed = JSON.parse response.body
    assert_response :success
    assert_not_empty response_parsed['message']['videos'][0]
  end

  test 'should return unread messages' do
    create(:message, user_id: @user.id, chat_id: @chat.id)
    get :index, chat_id: @chat.id, unread: '1'
    response_parsed = JSON.parse response.body
    assert_response :success
    assert_equal response_parsed['messages'].length, 2
  end

  test 'should return read messages' do
    get :index, chat_id: @chat.id
    create(:message, user_id: @user.id, chat_id: @chat.id)
    get :index, chat_id: @chat.id, unread: '0'
    response_parsed = JSON.parse response.body
    assert_response :success
    assert_equal response_parsed['messages'].length, 1
  end

  test 'should convert video to different formats and resolution' do
    body = "http://www.engr.colostate.edu/me/facil/dynamics/files/drop.avi"
    post :create, chat_id: @chat.id, body: body
    response_parsed = JSON.parse response.body
    assert_response :success
    assert_not_empty response_parsed['message']['videos'][0]
    assert_not_empty response_parsed['message']['videos'][0]['mp4_320_240']
    assert_not_empty response_parsed['message']['videos'][0]['mp4_480_270']
    assert_not_empty response_parsed['message']['videos'][0]['mp4_640_480']
    assert_not_empty response_parsed['message']['videos'][0]['mp4_1080_720']
    assert_not_empty response_parsed['message']['videos'][0]['mp4_1920_1080']
    assert_not_empty response_parsed['message']['videos'][0]['webm_320_240']
    assert_not_empty response_parsed['message']['videos'][0]['webm_480_270']
    assert_not_empty response_parsed['message']['videos'][0]['webm_640_480']
    assert_not_empty response_parsed['message']['videos'][0]['webm_1080_720']
    assert_not_empty response_parsed['message']['videos'][0]['webm_1920_1080']
  end

  test 'should remove old attachments' do
    body = "http://www.engr.colostate.edu/me/facil/dynamics/files/drop.avi"
    post :create, chat_id: @chat.id, body: body
    new_body =  "http://www.dotatalk.com/wp-content/uploads/2013/09/Logo-DOTA27.jpg"
    patch :update, chat_id: @chat.id, id: @message.id, body: new_body
    update_response = JSON.parse response.body
    assert_response :success
    assert_empty update_response['message']['videos']
  end

  test 'should full messsage search' do
    create(:chat, id: 1)
    create(:participant, user_id: @user.id, chat_id: 1)
    create(:message, body: 'Submit application till monday', user_id: @user.id, chat_id: @chat.id)
    create(:message, body: 'Application was submitted', user_id: @user.id, chat_id: @chat.id)
    create(:message, body: 'Submission was succussfull', user_id: @user.id, chat_id: @chat.id)
    get :index, chat_id: 1, query: 'submit'
    parsed_response = JSON.parse response.body
    assert_not_empty parsed_response['messages']
  end
end