require 'test_helper'
require 'ffaker'

class Api::V1::Chat::ParticipantsControllerTest < ActionController::TestCase

  setup do
    @chat = create(:chat)
    @user = create (:user)
    @session = create(:session, :user_id => @user.id)
    create(:participant, user_id: @user.id, chat_id: @chat.id)
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Token.encode_credentials(@session.token)
  end

  test "should show all participants" do
    post :index, chat_id: @chat.id
    assert_response :success
  end

  test "should return online participant" do
    $redis_onlines.flushdb
    another_user = create (:user)
    third_user = create (:user)
    session = create(:session, :user_id => another_user.id)
    post :create, user_id: another_user.id, chat_id: @chat.id
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Token.encode_credentials(session.token)
    post :create, user_id: third_user.id, chat_id: @chat.id
    get :index, chat_id: @chat.id, online: '1'
    participants = JSON.parse response.body
    assert_response :success
    assert_equal participants['participants'].length, 2
  end

  test "should return offline participant" do
    $redis_onlines.flushdb
    another_user = create (:user)
    third_user = create (:user)
    session = create(:session, :user_id => another_user.id)
    post :create, user_id: another_user.id, chat_id: @chat.id
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Token.encode_credentials(session.token)
    post :create, user_id: third_user.id, chat_id: @chat.id
    get :index, chat_id: @chat.id, online: '0'
    participants = JSON.parse response.body
    assert_response :success
    assert_equal participants['participants'].length, 1
  end

  test "should add participant to chat" do
    post :create, user_id: @user.id, chat_id: @chat.id
    assert_response :success
  end

  test "shouldn't add participant to chat if wrong params" do
    post :create, wrong_par1: @user.id, chat_id: @chat.id
    assert_response :unprocessable_entity
  end

  test "shouldn't add participant to chat if chat don't exist" do
    post :create, chat_id: FFaker::Address.building_number
    assert_response :not_found
  end
end