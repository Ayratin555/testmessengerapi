require 'uri'
require 'net/http'

class MessageScraperService

  def self.attachments(params)
    @body = params[:body]
    scrape_urls
    { urls_attributes: @urls, images_attributes: @images, videos_attributes: @videos }
  end

  private

    def self.scrape_urls
      @urls = []
      @images = []
      @videos = []
      @body.scan(URI.regexp) do
        match = $&
        unless match_media(match)
          url = {}
          url['url_adr'] = match
          @urls.append(url)
        end
      end if @body
      scrape_info
    end
  

    def self.match_media(string)
      match_image_url(string) || match_video_url(string)
    end

    def self.match_image_url(string)
      if string.match(/[-_\w:]+\.(jpe?g|png|gif)$/i)
        url = {}
        url['url'] = string
        @images.append(url)
        true
      else
        false
      end
    end

    def self.match_video_url(string)
      if string.match(/[-_\w:]+\.(avi|flv|wmv|mov|mp4|mpeg)$/i)
        url ={}
        url['url'] = string
        @videos.append(url)
        true
      else
        false
      end
    end

    def self.scrape_info
      @urls.each do |url|
        if url_exist?(url['url_adr'])
          inspector = MetaInspector.new(url['url_adr'])
          url['name'] = inspector.best_title
          url['description'] = inspector.description
          url['image_url'] = inspector.images.best
        end
      end
    end

    def self.url_exist?(url_string)
      url = URI.parse(url_string)
      req = Net::HTTP.new(url.host, url.port)
      req.use_ssl = (url.scheme == 'https')
      path = url.path if url.path.present?
      res = req.request_head(path || '/')
      res.code != "404"
    rescue Errno::ECONNREFUSED
      false
    end
end