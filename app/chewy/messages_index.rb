class MessagesIndex < Chewy::Index
  settings analysis: {
               analyzer: {
                   body: {
                       tokenizer: 'standard',
                       filter: ['lowercase']
                   }
               }
           }

  define_type Chat::Message do
    field :body, analyzer: 'body'
    field :chat_id, type: 'integer'
    field :user_id, type: 'integer'
    field :is_deleted
    field :created_at, type: 'date'
  end
end