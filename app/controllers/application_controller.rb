class ApplicationController < ActionController::Base
  protected

    def authenticate
      authenticate_or_request_with_http_token do |token, options|
        @current_session = Session.find_and_check_by_token(token: token)
      end
    end

    def current_user
      @current_session.user
    end

    class ChatNotFound < RuntimeError; end
    class MessageNotFound < RuntimeError; end
    class SessionNotFound < RuntimeError; end
    class UserNotFound < RuntimeError; end
    class UserNotParticipant < RuntimeError; end
    class NotUsersMessage < RuntimeError; end
end
