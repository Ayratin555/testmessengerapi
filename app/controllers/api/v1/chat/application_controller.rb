class Api::V1::Chat::ApplicationController < Api::V1::ApplicationController

  protected

    def chat!
      @chat ||= find_chat!(params[:chat_id])
    end

    def message!
      @message ||= find_message!(params[:id])
      @message
    end

    def find_chat! (chat_id)
      chat = ::Chat.find_by_id(chat_id) || raise(ChatNotFound.new)
      raise(UserNotParticipant.new) unless current_user.chats.include?(chat)
      chat
    end

    def find_message! (message_id)
      message = chat!.messages.find_by_id(message_id) || raise(MessageNotFound.new)
      raise(NotUsersMessage.new) unless current_user.messages.exists?(id: message.id)
      message
    end

    def set_user_param
      params[:user_id] = current_user.id
    end

    def parse_urls
      attachments = MessageScraperService.attachments(params)
      params[:urls_attributes] = attachments[:urls_attributes]
      params[:videos_attributes] = attachments[:videos_attributes]
      params[:images_attributes] = attachments[:images_attributes]
    end


    def participants!
      participants = chat!.participants
      participants = participants.where(user_id: $redis_onlines.keys) if (params[:online]=='1')
      participants = participants.where.not(user_id: $redis_onlines.keys) if (params[:online]=='0')
      participants
    end

    def messages!
      id = chat!.id
      messages = MessagesIndex::Message.filter{chat_id == id}
      messages = messages.query(query_string: {query: params[:query]}) if params[:query]
      messages = messages.filter{user_id == params[:user_id]} if params[:specific_user]
      messages = messages.filter{is_deleted == params[:is_deleted]} if params[:is_deleted]
      if params[:date]
        begin_date = Time.at(params[:date].to_i).beginning_of_day
        end_date = Time.at(params[:date].to_i).end_of_day
        messages = messages.filter{(created_at<=end_date) & (created_at>=begin_date)}
      end
      messages
    end
end