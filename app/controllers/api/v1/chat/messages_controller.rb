class Api::V1::Chat::MessagesController < Api::V1::Chat::ApplicationController
  before_action :authenticate, :set_user_param
  before_action :parse_urls, only: [:create, :update]

  def index
    messages = messages!.page params[:page]
    render json: messages
  end

  def create
    message = chat!.messages.build(message_params)
    if message.save
      render json: message
    else
      render json: message.errors, status: :unprocessable_entity
    end
  end

  def update
    message = message!
    if message.update(message_params)
      render json: message
    else
      render json: message.errors, status: :unprocessable_entity
    end
  end

  def destroy
    message!.mark_as_deleted
    render json: 'Message was deleted', status: :no_content
  end

  private

    def message_params
      params.permit(
          :body,
          :user_id,
          videos_attributes: [ :url ],
          images_attributes: [ :url ],
          urls_attributes: [ :url_adr, :name, :description, :image_url ] )
    end
end
