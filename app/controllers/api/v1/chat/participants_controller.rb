class Api::V1::Chat::ParticipantsController < Api::V1::Chat::ApplicationController
  before_action :authenticate

  def index
    participants = participants!.page params[:page]
    render json: participants
  end

  def create
    participant = participants!.build(participant_params)
    if participant.save
      render json: participant
    else
      render json: participant.errors, status: :unprocessable_entity
    end
  end

  private

    def participant_params
      params.permit(:user_id)
    end
end