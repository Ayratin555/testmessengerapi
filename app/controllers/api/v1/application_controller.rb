class Api::V1::ApplicationController < ApplicationController
  rescue_from ChatNotFound, :with => :render_chat_not_found
  rescue_from MessageNotFound, :with => :render_message_not_found
  rescue_from UserNotParticipant, :with => :render_user_not_participant
  rescue_from NotUsersMessage, :with => :not_users_message
  rescue_from UserNotFound, :with => :render_user_not_found
  rescue_from SessionNotFound, :with => :render_session_not_found
  after_action :set_online

  def user!
    @user ||= find_user!(params[:email], params[:password])
  end

  def session!
    @session ||= find_session!(params[:email], params[:password], params[:id])
  end

  def find_user!(email, password)
    User::find_by_email_and_password(email, password) || raise(UserNotFound.new)
  end

  def set_ip_param
    params['ip_address'] = request.remote_ip
  end

  def find_session!(email, password, token)
    user = find_user!(email, password)
    user.sessions.find_by(token: token) || raise(SessionNotFound.new)
  end

  protected

    def set_online
      $redis_onlines.set( current_user.id, nil, ex: 10*60 ) unless current_user.nil?
    end

  private
    def render_user_not_found
      render json: 'errors: User not found ', status: :not_found
    end

    def render_chat_not_found
      render json: 'errors: Chat not found ', status: :not_found
    end

    def render_message_not_found
      render json: 'errors: Message not found', status: :not_found
    end

    def session_not_found
      render json: 'errors: Session not found', status: :not_found
    end

    def render_user_not_participant
      render json: 'errors: User not participant of this chat', status: :unauthorized
    end

    def not_users_message
      render json: "errors: Not user's message", status: :unauthorized
    end
end