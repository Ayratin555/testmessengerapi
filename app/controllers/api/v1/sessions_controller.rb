class Api::V1::SessionsController < Api::V1::ApplicationController
  before_action :set_ip_param, only: [:create]
  skip_after_action :set_online

  def create
    session = user!.sessions.build(session_params)
    if session.save
      render json: session
    end
  end

  def destroy
    session!.destroy
    render json: 'Session was deleted', status: :no_content
  end

  private

    def session_params
      params.permit(:ip_address)
    end
end