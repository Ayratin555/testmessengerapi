class Api::V1::ChatsController < Api::V1::ApplicationController
  before_action :authenticate

  def index
    chats = Kaminari.paginate_array(current_user.chats).page(params[:page])
    render json: chats
  end

  def create
    chat = ::Chat.new(chat_params)
    if chat.save
      chat.add_user_to_participants(current_user.id)
      render json: chat
    else
      render json: chat.errors, status: :unprocessable_entity
    end
  end

  private

    def chat_params
      params.permit(:name)
    end
end
