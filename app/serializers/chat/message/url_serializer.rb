class Chat::Message::UrlSerializer < ActiveModel::Serializer
  attributes :url_adr, :name, :description, :image_original, :image_thumb

  def image_original
    object.image.url
  end

  def image_thumb
    object.image.url(:thumb)
  end
end