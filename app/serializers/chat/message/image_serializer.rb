class Chat::Message::ImageSerializer < ActiveModel::Serializer
  attributes :image_original, :image_thumb

  def image_original
    object.image.url
  end

  def image_thumb
    object.image.url(:thumb)
  end
end