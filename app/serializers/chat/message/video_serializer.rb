class Chat::Message::VideoSerializer < ActiveModel::Serializer
  attributes :url, :mp4_320_240, :mp4_480_270, :mp4_640_480,
             :mp4_1080_720, :mp4_1920_1080,
             :webm_320_240, :webm_480_270, :webm_640_480,
             :webm_1080_720, :webm_1920_1080,
             :preview_image, :thumb_image

  def mp4_320_240
    object.video.url(:mp4_320_240)
  end

  def mp4_480_270
    object.video.url(:mp4_480_270)
  end

  def mp4_640_480
    object.video.url(:mp4_640_480)
  end

  def mp4_1080_720
  object.video.url(:mp4_1080_720)
  end

  def mp4_1920_1080
    object.video.url(:mp4_1920_1080)
  end

  def webm_320_240
    object.video.url(:webm_320_240)
  end

  def webm_480_270
    object.video.url(:webm_480_270)
  end

  def webm_640_480
    object.video.url(:webm_640_480)
  end

  def webm_1080_720
    object.video.url(:webm_1080_720)
  end

  def webm_1920_1080
    object.video.url(:webm_1920_1080)
  end

  def preview_image
    object.video.url(:preview)
  end

  def thumb_image
    object.video.url(:thumb)
  end
end