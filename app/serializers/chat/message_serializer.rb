class Chat::MessageSerializer < ActiveModel::Serializer
  attributes :id, :body, :created_at, :updated_at
  has_one :user
  has_many :urls
  has_many :images
  has_many :videos
end
