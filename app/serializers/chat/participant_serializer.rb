class Chat::ParticipantSerializer < ActiveModel::Serializer
  attributes :created_at
  has_one :user
end
