class Session < ActiveRecord::Base
  belongs_to :user
  validates :user, presence: true
  before_create :set_token
  after_create :remove_old_sessions

  def set_token
    self.token = generate_token
  end

  def self.find_and_check_by_token(token)
    session = Session.find_by(token)
    if session
      if session.created_at.to_i + interval_to_seconds(session.expiration) > Time.now.to_i
        return session
      end
    end
    nil
  end

  private

    def self.interval_to_seconds (interval)
      units = %w{days hours minutes seconds}
      interval.split(":").map.with_index{|x,i| x.to_i.send(units[i])}.reduce(:+).to_i
    end

    def generate_token
      SecureRandom.hex
    end

    def remove_old_sessions
      self.user.sessions.where('created_at < ?', self.created_at).destroy_all
    end
end