class Chat::Message < ActiveRecord::Base
  has_many :urls, dependent: :destroy, :foreign_key => 'chat_message_id', :inverse_of => :message
  has_many :images, dependent: :destroy, :foreign_key => 'chat_message_id', :inverse_of => :message
  has_many :videos, dependent: :destroy, :foreign_key => 'chat_message_id', :inverse_of => :message
  accepts_nested_attributes_for :urls
  accepts_nested_attributes_for :images
  accepts_nested_attributes_for :videos
  validates :body, presence: true, length: { maximum: 400 }
  validates :user, :chat, presence: true
  belongs_to :user
  belongs_to :chat
  after_update :remove_old_attachments
  default_scope -> { order('created_at DESC') }
  acts_as_readable :on => :created_at
  update_index 'messages', 'self', urgent: true

  scope :equal_to_date, lambda { |date| where('created_at >= ? AND created_at <= ?', date.beginning_of_day, date.end_of_day) }
  scope :by_user, lambda { |user_id| where('user_id = ?', user_id) }
  scope :is_deleted, lambda { |flag| where(is_deleted: flag) }

  def mark_as_deleted
    self.update(is_deleted: true)
  end



  private

    def remove_old_attachments
      self.urls.where('created_at < ?', self.updated_at).destroy_all
      self.videos.where('created_at < ?', self.updated_at).destroy_all
      self.images.where('created_at < ?', self.updated_at).destroy_all
    end
end
