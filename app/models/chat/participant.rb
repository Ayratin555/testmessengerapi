class Chat::Participant < ActiveRecord::Base
  belongs_to :user
  belongs_to :chat
  validates :user, :chat, presence: true

  def self.search(params = {})
    messages = Chat::Message.all
    messages = messages.by_user(params[:user_id]) if params[:user_id]
    if params[:deleted]
      is_deleted =  params[:deleted] == '1'
      messages = messages.is_deleted(is_deleted)
    end
    messages = messages.equal_to_date(Time.at(params[:date].to_i)) if params[:date]
    messages
  end
end
