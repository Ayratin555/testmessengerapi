class Chat::Message::Image < ActiveRecord::Base
  validates :message, :url, presence: true
  has_attached_file :image, styles: {thumb: "200x200#"}
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  belongs_to :message, :foreign_key => 'chat_message_id'
  after_create :attach_image

  private
    def attach_image
      self.image = URI.parse(self.url)
      self.save
    end
end