class Chat::Message::Video < ActiveRecord::Base
  validates :message, :url, presence: true
  has_attached_file :video, :styles => {
                                               :mp4_320_240 => { format: 'mp4',
                                                                   convert_options: {
                                                                       output: { strict: 'experimental',
                                                                                 s: '320x240#' }
                                                                   }
                                               },
                                               :mp4_480_270 => { format: 'mp4',
                                                                   convert_options: {
                                                                       output: { strict: 'experimental',
                                                                                 s: '480x270#' }
                                                                   }
                                               },

                                                :mp4_640_480 => { format: 'mp4',
                                                              convert_options: {
                                                                  output: { strict: 'experimental',
                                                                            s: '640x480#' }
                                                              }
                                                },
                                                :mp4_1080_720 =>  { format: 'mp4',
                                                                     convert_options: {
                                                                         output: { strict: 'experimental',
                                                                                   s: '1080x720#'  }
                                                                     }
                                                },
                                                :mp4_1920_1080 => { format: 'mp4',
                                                          convert_options: {
                                                              output: { strict: 'experimental',
                                                                        s: '1920x1080#' }
                                                          }
                                                },
                                                :webm_320_240 => {
                                                    :format => 'webm',
                                                    :geometry => '320x240#',
                                                    :convert_options => {
                                                        :input => {},
                                                        :output => {
                                                            :vcodec => 'libvpx',
                                                            :acodec => 'libvorbis',
                                                            'cpu-used' => -10,
                                                            :deadline => :realtime,
                                                            :strict => :experimental
                                                        }
                                                    }
                                                },
                                                :webm_480_270  => {
                                                    :format => 'webm',
                                                    :geometry => '480x270#',
                                                    :convert_options => {
                                                        :input => {},
                                                        :output => {
                                                            :vcodec => 'libvpx',
                                                            :acodec => 'libvorbis',
                                                            'cpu-used' => -10,
                                                            :deadline => :realtime,
                                                            :strict => :experimental
                                                        }
                                                    }
                                                },
                                                :webm_640_480 => {
                                                    :format => 'webm',
                                                    :geometry => '720x480#',
                                                    :convert_options => {
                                                        :input => {},
                                                        :output => {
                                                            :vcodec => 'libvpx',
                                                            :acodec => 'libvorbis',
                                                            'cpu-used' => -10,
                                                            :deadline => :realtime,
                                                            :strict => :experimental
                                                        }
                                                    }
                                                },
                                                :webm_1080_720 => {
                                                    :format => 'webm',
                                                    :geometry => '1080x720#',
                                                    :convert_options => {
                                                        :input => {},
                                                        :output => {
                                                            :vcodec => 'libvpx',
                                                            :acodec => 'libvorbis',
                                                            'cpu-used' => -10,
                                                            :deadline => :realtime,
                                                            :strict => :experimental
                                                        }
                                                    }
                                                },
                                                :webm_1920_1080 => {
                                                    :format => 'webm',
                                                    :geometry => '1920x1080#',
                                                    :convert_options => {
                                                        :input => {},
                                                        :output => {
                                                            :vcodec => 'libvpx',
                                                            :acodec => 'libvorbis',
                                                            'cpu-used' => -10,
                                                            :deadline => :realtime,
                                                            :strict => :experimental
                                                        }
                                                    }
                                                },
                                                :preview => {
                                                    :format => :jpg,
                                                    :geometry => '1200x675#',
                                                    :convert_options => {
                                                        :output => {
                                                            :vframes => 1,
                                                            :s => '1200x675',
                                                            :ss => '00:00:02'
                                                        }
                                                    }
                                                },
                                                :thumb => {
                                                    :format => :jpg,
                                                    :geometry => '300x169#',
                                                    :convert_options => {
                                                        :output => {
                                                            :vframes => 1,
                                                            :s => '300x169',
                                                            :ss => '00:00:02'
                                                        }
                                                    }
                                                },
                                            },
                                      :processors => [:transcoder]
  validates_attachment_size :video, less_than: 1.gigabytes
  validates_attachment_content_type :video, :content_type => ["video/mp4", "video/quicktime", "video/x-flv", "video/x-msvideo", "video/x-ms-wmv", "video/webm", "video/avi"]
  process_in_background :video
  belongs_to :message, :foreign_key => 'chat_message_id'
  after_create :attach_video

  private
  def attach_video
    self.video = URI.parse(self.url)
    self.save
  end
end