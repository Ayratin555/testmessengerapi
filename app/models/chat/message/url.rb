class Chat::Message::Url < ActiveRecord::Base
  validates :url_adr, :message, presence: true
  has_attached_file :image, styles: {thumb: "200x200#"}
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  belongs_to :message, :foreign_key => 'chat_message_id'
  after_create :add_image_url

  private
    def add_image_url
      self.image = URI.parse(self.image_url) unless self.image_url.nil?
      self.save
    end
end