class Chat::Image < ActiveRecord::Base
  validates :user, :chat, presence: true
  belongs_to :user
  belongs_to :chat
  has_attached_file :image, styles: {thumb: "200x200#"}
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
end