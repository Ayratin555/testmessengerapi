class User < ActiveRecord::Base
  has_many :messages, :class_name => 'Chat::Message'
  has_many :chats, through: :participants
  has_many :sessions
  validates :name, :email, presence: true, uniqueness: true
  validates :password, presence: true
  acts_as_reader

  def chats
    chats = []
    participant_of = Chat::Participant.where(user: self.id)
    unless participant_of.nil?
      chat_ids = participant_of.map{|part| part.chat_id}
      chats = Chat.find(chat_ids)
    end
    chats
  end

  private

    def generate_token
      SecureRandom.hex
    end
end