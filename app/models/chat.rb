class Chat < ActiveRecord::Base
  has_many :messages, dependent: :destroy
  has_many :participants, dependent: :delete_all
  has_many :users, through: :participants
  validates :name, presence: true

  def add_user_to_participants(user_id)
    participant = ::Chat::Participant.new
    participant.user_id = user_id
    participant.chat_id = self.id
    participant.save
  end
end
